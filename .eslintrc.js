module.exports = {
    extends: ["airbnb-typescript"],
    parserOptions: {
        project: "./tsconfig.json",
    },
    settings: {
        "import/resolver": {
            "node": {
                "paths": ["src"]
            }
        },
        "react": {
            "version": "-1"
        }
    },
    rules: {
        "default-case": "off",
        "import/prefer-default-export": "off",
        "max-len": ["warn", { "code": 120 }],
        "no-plusplus": "off",
        "operator-linebreak": ["error", "after", { "overrides": { "?": "before", ":": "before" } }],
        "object-curly-newline": ["error", {
            "ObjectExpression": { "multiline": true, "minProperties": 4 },
            "ObjectPattern": { "multiline": true },
            "ImportDeclaration": { "multiline": true },
            "ExportDeclaration": { "multiline": true, "minProperties": 3 }
        }],
        "semi": "off",
        "@typescript-eslint/comma-dangle": ["error", {
            "arrays": "never",
            "objects": "never",
            "imports": "never",
            "exports": "never",
            "functions": "never"
        }],
        "@typescript-eslint/comma-spacing": "off",
        "@typescript-eslint/indent": ["error", 4],
        "@typescript-eslint/no-explicit-any": ["error"],
        "@typescript-eslint/quotes": ["error", "double"],
        "@typescript-eslint/semi": "error",
        "@typescript-eslint/typedef": ["error", { "variableDeclaration": true}]
    }
};
