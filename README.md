# OAuth2/Data

The Data service is meant as a single entrance point for frontends to provide data fetching, writing endpoints, and to authorize users and requests.

## Quick start
Rename `/example.env` to `/.env` and fill in the placeholder values. You will need a set of OAuth2/auth client information to establish the connection.
```bash
npm install
```
followed by
```bash
npm run start
```
## Usage

### Setup
First you are required to have a set of valid OAuth2/auth client credentials.
Those are written to the .env variables and used to authorize all user related requests:
* AUTH_CLIENT_ID
* AUTH_URL
* AUTH_ISSUER_NAME
* AUTH_AUDIENCE_NAME

Additionally, you will receive a private key as well, this needs to be placed in `/src/assets/keys/auth_client_private.pem`. See [known issues](#known-issues) for more information.

After that you need to edit the CORS_WHITELIST .env variable to allow frontends to access this service.
And now you are already done setting up and starting this service.

### Endpoints
There are endpoints available to handle all kind of user authorization and some data endpoints at the moment.

#### User authorization
##### /auth/login
This endpoint allows the frontend to log in with a user.
The request needs to be a `POST` request and provide the `username` and the hashed `password` as parameters and will be forwarded to the OAuth service.
If the login succeeds it will return an `accessToken` and `refreshToken` with the status `201`.
Otherwise, the status `400` or `401` will be returned.

##### /auth/refresh
This endpoint allows the frontend to refresh an expired `accessToken`.
The request needs to be a `POST` request and provide an existing `refreshToken` as parameter and will be forwarded to the OAuth service.
If the refresh succeeds it will return a new set of `accessToken` and `refreshToken` with the status `201`.
If the `accessToken` related to the provided `refreshToken` is not expired yet, it will return the existing set of `accessToken` and `refreshToken` with the status `200`.
Otherwise, the status `400` or `401` will be returned.

##### /auth/invalidate
This endpoint allows to invalidate a set of `accessToken` and `refreshToken`.
The request needs to be a `POST` request and produce an existing `refreshToken` as parameter and will be forwarded to the OAuth service.
If the invalidation succeeds it will return the status `200`. Otherwise, the status `400` will be returned.

#### Data fetching
Once the user is logged in and received a valid accessToken, the frontend can query for data. There are CRUD endpoints for the following resources:
* Users

To query any of the endpoints it is required to send an `Authorization` header with a `bearer token` based on the accessToken.

## Extensibility
Since this whole project was mainly based on learning as you can see in the **Oauth2/auth** README.md section **Motivation**,
this project came a bit on hold after the main part, the authorization, was successfully implemented and working.
The project may or may not be extended at some point, for the moment its on hold and only the authorization and user endpoints are available.
It could be extended to get a complete user and client management running.
Once it is decided for which kind of product it should be used,
any new endpoints for data fetching and manipulation could be added as well to get a single entrance point for frontends.

## Known issues
The public key of OAuth2/auth and the private key of the OAuth2/auth client configuration are currently stored as a file in /src/assets/keys which is totally insecure.
Before using the service in any near production-like environment, those keys need to be outsourced into any kind of secure key storage.
They are just stored there as a key during the initial development to make the development easier and more comfortable.

#### Author

* [Lars Bomnüter](mailto:larsbomnueter@web.de)

---

MIT License
