import * as core from "express";
import { readFileSync } from "fs";
import nodeFetch from "node-fetch";
import { sign } from "../utils/crypto";
import { RequestWithCookies, Route } from "../types/routes";
import { ApiRequestMethods, ApiResponse } from "../types/api";
import { LoginResponseWithError } from "../types/auth";
import { handleResponseError, parseResponse } from "../utils/api";
import { sendLoginResponse } from "../utils/auth";

export const login: Route = async (req: RequestWithCookies, res: core.Response) => {
    const authClientPrivateKey: string = readFileSync("src/assets/keys/auth_client_private.pem", "utf8");
    const clientSecret: string = sign(process.env.AUTH_CLIENT_ID, authClientPrivateKey);

    const responseRaw: ApiResponse<LoginResponseWithError> = await nodeFetch(
        `${process.env.AUTH_URL}/auth`,
        {
            method: ApiRequestMethods.POST,
            body: JSON.stringify({
                username: req.body?.username,
                password: req.body?.password,
                grant_type: "password",
                client_id: process.env.AUTH_CLIENT_ID,
                client_secret: clientSecret
            }),
            headers: { "Content-Type": "application/json" }
        }
    )
        .then(parseResponse)
        .catch(handleResponseError);

    sendLoginResponse(responseRaw, res);
};

export const refresh: Route = async (req: RequestWithCookies, res: core.Response) => {
    const authClientPrivateKey: string = readFileSync("src/assets/keys/auth_client_private.pem", "utf8");
    const clientSecret: string = sign(process.env.AUTH_CLIENT_ID, authClientPrivateKey);

    const responseRaw: ApiResponse<LoginResponseWithError> = await nodeFetch(
        `${process.env.AUTH_URL}/refresh`,
        {
            method: ApiRequestMethods.POST,
            body: JSON.stringify({
                refreshToken: req.body?.refreshToken,
                grant_type: "password",
                client_id: process.env.AUTH_CLIENT_ID,
                client_secret: clientSecret
            }),
            headers: { "Content-Type": "application/json" }
        }
    )
        .then(parseResponse)
        .catch(handleResponseError);

    sendLoginResponse(responseRaw, res);
};

export const invalidate: Route = async (req: RequestWithCookies, res: core.Response) => {
    const authClientPrivateKey: string = readFileSync("src/assets/keys/auth_client_private.pem", "utf8");
    const clientSecret: string = sign(process.env.AUTH_CLIENT_ID, authClientPrivateKey);

    nodeFetch(
        `${process.env.AUTH_URL}/invalidate`,
        {
            method: ApiRequestMethods.POST,
            body: JSON.stringify({
                refreshToken: req.body?.refreshToken,
                grant_type: "password",
                client_id: process.env.AUTH_CLIENT_ID,
                client_secret: clientSecret
            }),
            headers: { "Content-Type": "application/json" }
        }
    )
        // ignore the response since the client can't fix it in any way if something fails and the user should not be
        // confused
        .then(() => {})
        .catch(() => {});

    res.sendStatus(200);
};
