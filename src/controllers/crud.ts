import * as core from "express";
import { readFileSync } from "fs";
import nodeFetch from "node-fetch";
import { RequestWithCookies, RouteCreator } from "../types/routes";
import { verifyAuthorization } from "../utils/auth";
import { sign } from "../utils/crypto";
import { ApiRequestMethods, ApiResponse } from "../types/api";
import { LoginResponseWithError } from "../types/auth";
import { handleResponseError, parseResponse } from "../utils/api";

const genericRoute: (
    req: RequestWithCookies,
    res: core.Response,
    url: string,
    includeId: boolean,
    method: ApiRequestMethods,
    includeBody: boolean
) => void = async (
    req: RequestWithCookies,
    res: core.Response,
    url: string,
    includeId: boolean,
    method: ApiRequestMethods,
    includeBody: boolean
) => {
    const authorization: string = req.get("Authorization");

    if (!verifyAuthorization(authorization)) {
        res.type("application/json").status(401).json({ error: "unauthorized" });
        return;
    }

    const authClientPrivateKey: string = readFileSync("src/assets/keys/auth_client_private.pem", "utf8");
    const clientSecret: string = sign(process.env.AUTH_CLIENT_ID, authClientPrivateKey);
    const id: string | undefined = req.params?.id;

    const responseRaw: ApiResponse<LoginResponseWithError> = await nodeFetch(
        `${url}${includeId && id ? `/${id}` : ""}`,
        {
            method,
            headers: {
                Authorization: authorization,
                "Content-Type": "application/json",
                "X-Client-Id": process.env.AUTH_CLIENT_ID,
                "X-Client-Secret": clientSecret
            },
            body: includeBody ? JSON.stringify(req.body) : undefined
        }
    )
        .then(parseResponse)
        .catch(handleResponseError);

    res.status(responseRaw?.status || 500).send(responseRaw?.data || responseRaw?.error);
};

export const createRoute: RouteCreator = (url: string) => async (req: RequestWithCookies, res: core.Response) => {
    genericRoute(req, res, url, false, ApiRequestMethods.POST, true);
};

export const readRoute: RouteCreator = (url: string) => async (req: RequestWithCookies, res: core.Response) => {
    genericRoute(req, res, url, true, ApiRequestMethods.GET, false);
};

export const updateRoute: RouteCreator = (url: string) => async (req: RequestWithCookies, res: core.Response) => {
    genericRoute(req, res, url, false, ApiRequestMethods.PUT, true);
};

export const deleteRoute: RouteCreator = (url: string) => async (req: RequestWithCookies, res: core.Response) => {
    genericRoute(req, res, url, true, ApiRequestMethods.DELETE, false);
};
