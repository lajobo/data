import { config as setupDotenv } from "dotenv";
import { initializeRoutes } from "./router/router";

setupDotenv();
initializeRoutes();
