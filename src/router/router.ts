import * as core from "express-serve-static-core";
import express, { NextFunction, Request, Response } from "express";
import cors from "cors";
import compression from "compression";
import cookiesMiddleware from "universal-cookie-express";
import bodyParser from "body-parser";
import { log } from "../utils/logger";
import { LogLevels } from "../types/logger";
import { errorRoute } from "../controllers/error";
import { accessLogMiddleware } from "./middlewares/accessLog";
import { invalidate, login, refresh } from "../controllers/auth";
import { registerCRUDRoutes } from "../utils/route";

export const initializeRoutes: () => void = () => {
    const app: core.Express = express();

    app.use(compression());
    app.use(express.json());
    app.use(cors({ origin: process.env.CORS_WHITELIST }));
    app.use(cookiesMiddleware());
    app.use(accessLogMiddleware);
    app.use(bodyParser.json());
    app.use(bodyParser.urlencoded({ extended: false }));

    app.disable("x-powered-by");

    app.post("/auth/login", login);
    app.post("/auth/refresh", refresh);
    app.post("/auth/invalidate", invalidate);

    registerCRUDRoutes(app, "users", `${process.env.AUTH_URL}/users`, "crud");

    // Express requires 4 arguments in the route to detect it as an error route
    /* eslint-disable @typescript-eslint/no-unused-vars */
    // noinspection JSUnusedLocalSymbols
    app.use((error: Error, req: Request, res: Response, next: NextFunction) => {
        errorRoute(req, res, error);
    });
    /* eslint-enable @typescript-eslint/no-unused-vars */

    app.listen(process.env.PORT, () => {
        log(LogLevels.Info, `Webserver started at port ${process.env.PORT}`);
    });
};
