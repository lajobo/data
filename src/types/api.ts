export enum ApiRequestMethods {
    POST = "POST",
    GET = "GET",
    DELETE = "DELETE",
    PUT = "PUT"
}

export interface ApiResponse <T = unknown> {
    data: T;
    status: number;
    error?: any; // eslint-disable-line @typescript-eslint/no-explicit-any
}

export interface ApiResponseDataWithError {
    error: string;
}
