export interface LoginResponse {
    accessToken: string;
    refreshToken: string;
}

export interface LoginErrorResponse {
    error: string;
}

export type LoginResponseWithError = LoginResponse | LoginErrorResponse;

export interface LoginResponseVerification {
    result: boolean;
    error?: any; // eslint-disable-line @typescript-eslint/no-explicit-any
}
