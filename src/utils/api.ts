import { Response } from "node-fetch";
import { ApiResponse, ApiResponseDataWithError } from "../types/api";

export const parseResponse: <T extends ApiResponseDataWithError>(
    resp: Response
) => Promise<ApiResponse<T>> = async <T extends ApiResponseDataWithError>(
    resp: Response
) => {
    let data: T;

    try {
        if (resp.headers.get("content-type").startsWith("application/json")) {
            data = await resp.json();
        }
    } catch (e: unknown) { /* ignore if not parsable */ }

    if (typeof data === "object" && Object.prototype.hasOwnProperty.call(data, "error")) {
        return ({
            data: undefined,
            status: resp.status,
            error: (data as ApiResponseDataWithError).error
        });
    }

    return {
        data,
        status: resp.status
    } as ApiResponse<T>;
};

export const handleResponseError: (
    error: { name?: unknown }
) => ApiResponse<undefined> = (
    error: { name?: unknown }
) => ({
    data: undefined,
    status: 500,
    error: error?.name
});
