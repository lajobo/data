import { verify } from "jsonwebtoken";
import { readFileSync } from "fs";
import * as core from "express";
import { ApiResponse } from "../types/api";
import { LoginResponse, LoginResponseVerification, LoginResponseWithError } from "../types/auth";

export const verifyLoginResponseTokens: (
    loginResponse: ApiResponse<LoginResponse>
) => LoginResponseVerification = (
    loginResponse: ApiResponse<LoginResponse>
) => {
    const authPublicKey: string = readFileSync("src/assets/keys/auth_public.pem", "utf8");

    if (
        !loginResponse?.data ||
        !loginResponse?.data?.accessToken ||
        !loginResponse?.data?.refreshToken ||
        typeof loginResponse?.data?.accessToken !== "string" ||
        typeof loginResponse?.data?.refreshToken !== "string"
    ) {
        return {
            result: false,
            error: "token_verification_failure"
        };
    }
    try {
        verify(
            loginResponse.data.accessToken as string,
            authPublicKey,
            { audience: process.env.AUTH_AUDIENCE_NAME, issuer: process.env.AUTH_ISSUER_NAME }
        );
        verify(
            loginResponse.data.refreshToken as string,
            authPublicKey,
            { audience: process.env.AUTH_AUDIENCE_NAME, issuer: process.env.AUTH_ISSUER_NAME }
        );
    } catch (error: any) { // eslint-disable-line @typescript-eslint/no-explicit-any
        return {
            result: false,
            error: error?.name
        };
    }

    return { result: true };
};

export const sendLoginResponse: (
    responseRaw: ApiResponse<LoginResponseWithError>,
    res: core.Response
) => void = (
    responseRaw: ApiResponse<LoginResponseWithError>,
    res: core.Response
) => {
    let { status, error } = responseRaw;

    if (responseRaw?.status === 201 && !responseRaw.error) {
        const loginResponse: ApiResponse<LoginResponse> = responseRaw as ApiResponse<LoginResponse>;
        const loginResponseVerification: LoginResponseVerification = verifyLoginResponseTokens(loginResponse);

        if (!loginResponseVerification.result) {
            status = 500;
            error = loginResponseVerification.error;
        }
    }

    res.status(status || 500).send(responseRaw?.data || error);
};

export const verifyAuthorization: (authorization: string) => boolean = (authorization: string) => {
    if (
        !authorization ||
        !authorization.startsWith("Bearer ") ||
        authorization.length <= "Bearer ".length
    ) {
        return false;
    }

    const accessToken: string = authorization.substr("Bearer ".length);
    const authPublicKey: string = readFileSync("src/assets/keys/auth_public.pem", "utf8");

    try {
        verify(
            accessToken,
            authPublicKey,
            { audience: process.env.AUTH_AUDIENCE_NAME, issuer: process.env.AUTH_ISSUER_NAME }
        );
        return true;
    } catch (error: any) { // eslint-disable-line @typescript-eslint/no-explicit-any
        return false;
    }
};
