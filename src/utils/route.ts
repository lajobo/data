import * as core from "express-serve-static-core";
import { createRoute, deleteRoute, readRoute, updateRoute } from "../controllers/crud";

type Endpoints =
    "c" | "r" | "u" | "d" |
    "cr" | "cu" | "cd" | "ru" | "rd" | "ud" |
    "cru" | "crd" | "cud" | "rud" |
    "crud";

export const registerCRUDRoutes: (
    app: core.Express,
    path: string,
    targetUrl: string,
    endpoints: Endpoints
) => void = (
    app: core.Express,
    path: string,
    targetUrl: string,
    endpoints: Endpoints
) => {
    const prefixedPath: string = path.startsWith("/") ? path : `/${path}`;

    if (endpoints.includes("c")) {
        app.post(prefixedPath, createRoute(targetUrl));
    }

    if (endpoints.includes("r")) {
        app.get(prefixedPath, readRoute(targetUrl));
        app.get(`${prefixedPath}/:id`, readRoute(targetUrl));
    }

    if (endpoints.includes("u")) {
        app.put(prefixedPath, updateRoute(targetUrl));
    }

    if (endpoints.includes("d")) {
        app.delete(`${prefixedPath}/:id`, deleteRoute(targetUrl));
    }
};
